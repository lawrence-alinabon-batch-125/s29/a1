/*
	Graded Activity:

	1. Create a /home route that will return a string response "Welcome to the homepage" upon recieving a GET request.
	2. Hve the students create a /signup route that will "ADD" the user object {username, password} sent via the request body of a POST request to a users "ARRAY". Respond with a success notification ex: res.send (`User ${req.body.username} successfully registered!`) if BOTH username and password are provided/ not empty, else display a messafe res.send("Please input BOTH username and password.").

	3. Create /users endpoint that will return all the users/users array upon receiving GET request.
*/

let express = require("express");
const PORT = 4000;
let app = express();
let users = [];

app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get("/home", (req, res) => res.send(`Welcome to the home page`));

app.post("/signup", (req, res) => {

	if(req.body.username !== "" && req.body.password !== ""){

		let newUser = {
			"username" : req.body.username,
			"password" : req.body.password
		}
		users.push(newUser);
		res.send (`User ${req.body.username} successfully registered!`);

	}else{

		res.send("Please input BOTH username and password.");

	}

});

app.get("/users", (req, res) => res.send(users));

app.listen(PORT, ()=> {
	console.log(`Server running at port ${PORT}`)
});